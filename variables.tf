variable "sa-tags-division" {
  type = string
}

variable "sa-tags-org" {
  type = string
}

variable "sa-tags-keep-until" {
  type = string
}

variable "sa-tags-team" {
  type = string
}

variable "sa-tags-project" {
  type = string
}

variable "aws-region" {
  type = string
}