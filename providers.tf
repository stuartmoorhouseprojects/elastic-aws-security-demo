terraform {
  required_providers {
    ec = {
      source  = "elastic/ec"
      version = "0.9.0"
    }
    elasticstack = {
      source  = "elastic/elasticstack"
      version = "0.11.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "ec" {
}

provider "elasticstack" {
  elasticsearch {
    endpoints = [ec_deployment.elastic_cloud.elasticsearch.https_endpoint]
    username  = ec_deployment.elastic_cloud.elasticsearch_username
    password  = ec_deployment.elastic_cloud.elasticsearch_password
  }
  kibana {
    endpoints = [ec_deployment.elastic_cloud.kibana.https_endpoint]
  }
}
provider "aws" {
  region = var.aws-region
  default_tags {
    tags = {
      division   = var.sa-tags-division
      org        = var.sa-tags-org
      keep-until = var.sa-tags-keep-until
      team       = var.sa-tags-team
      project    = var.sa-tags-project
    }
  }
}


